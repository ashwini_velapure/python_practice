import unittest
import json
import requests




class MyTestCase(unittest.TestCase):
    def test_postRequest(self):
        url = 'https://httpbin.org/post'
        # Additional headers.
        headers = {'Content-Type': 'application/json'}
        # Body
        payload = {'key1': 1, 'key2': 2 }
        # convert dict to json by json.dumps() for body data.
        resp = requests.post(url, headers=headers, data=json.dumps(payload, indent=4))
        # Validate response headers and body contents, e.g. status code.
        assert resp.status_code == 200
        resp_body = resp.json()
        assert resp_body['url'] == url
        # print response full body as text
        print(resp.text)


    def test_getRequest(self):
        url = 'http://ip.jsontest.com/'
        resp = requests.get(url)
        assert resp.status_code == 200
        print(resp.text)





if __name__ == '__main__':
    unittest.main()
